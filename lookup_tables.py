def category_lookup() -> list:

    cat_lu = [
        ["Description", "Category", "TN Reporting Category", "GL Category"],
        ["Bonus Payment", "Stipends", "100-Wages", "1000-2000"],
        ["Stipend", "Stipends", "100-Wages", "1000-2000"],
        ["Stipend Lead Teacher", "Stipends", "100-Wages", "1000-2000"],
        ["Bonus Payment 2", "Stipends", "100-Wages", "1000-2000"],
        ["Stipend Recurring 1", "Stipends", "100-Wages", "1000-2000"],
        ["Regular Salary", "Wages", "100-Wages", "1000-2000"],
        ["Group Term Life", "Wages", "100-Wages", "1000-2000"],
        ["PTO Salaried Pre", "Wages", "100-Wages", "1000-2000"],
        ["Regular Pay-Hourly", "Wages", "100-Wages", "1000-2000"],
        ["OT Straight Time", "Wages", "100-Wages", "1000-2000"],
        ["Overtime Special", "Wages", "100-Wages", "1000-2000"],
        ["Retroactive OT", "Wages", "100-Wages", "1000-2000"],
        ["Retroactive Pay", "Wages", "100-Wages", "1000-2000"],
        ["Paid Time Off Pay Out", "Wages", "100-Wages", "1000-2000"],
        ["Holiday", "Wages", "100-Wages", "1000-2000"],
        ["Overtime", "Wages", "100-Wages", "1000-2000"],
        ["Jury Duty", "Wages", "100-Wages", "1000-2000"],
        ["Sick Time", "Wages", "100-Wages", "1000-2000"],
        ["Paid Time Off", "Wages", "100-Wages", "1000-2000"],
        ["Double OT Straight", "Wages", "100-Wages", "1000-2000"],
        ["Double Time Special Pay", "Wages", "100-Wages", "1000-2000"],
        ["Double Time", "Wages", "100-Wages", "1000-2000"],
        ["Taxable Benefit -  Medical", "Wages", "100-Wages", "1000-2000"],
        ["PTO Payout NO Decrement", "Wages", "100-Wages", "1000-2000"],
        ["Taxable Dental", "Wages", "100-Wages", "1000-2000"],
        ["Taxable Vision", "Wages", "100-Wages", "1000-2000"],
        ["Bereavement", "Wages", "100-Wages", "1000-2000"],
        ["Bereavement Salary", "Wages", "100-Wages", "1000-2000"],
        ["3rd Party Sick W2", "Wages", "100-Wages", "1000-2000"],
        ["Sick Salary", "Wages", "100-Wages", "1000-2000"],
        ["PTO Hourly Pre", "Wages", "100-Wages", "1000-2000"],
        ["3rd Party Sick No W2", "Wages", "100-Wages", "1000-2000"],
        ["Regular Special Pay", "Wages", "100-Wages", "1000-2000"],
        ["Regular no Social Security", "Wages", "100-Wages", "1000-2000"],
        ["Overtime @ 1.5", "Wages", "100-Wages", "1000-2000"],
        ["Resident Pay", "Wages", "100-Wages", "1000-2000"],
        ["PTO Salary", "Wages", "100-Wages", "1000-2000"],
        ["401K Percentage", "Wages", "100-Wages", "1000-2000"],
        ["403B TN Percentage", "Wages", "100-Wages", "1000-2000"],
        [
            "Social Security Employer Tax",
            "Medicare & Soc Sec",
            "201-Social Security",
            "3301-3302",
        ],
        [
            "State Teachers Retirement",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3110",
        ],
        [
            "State Teachers Retirement 62",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3110",
        ],
        [
            "Tennessee Consolidated Retirement System",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3115",
        ],
        [
            "TCRS Hybrid Great West",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3116",
        ],
        ["TCRS Hybrid", "Deferred Comp(USA)", "204-State Retirement", "3117"],
        [
            "Public Emp Retirement System (7%)",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3220",
        ],
        [
            "Public Emp Pension Reform Act 2013 (6%)",
            "Deferred Comp(USA)",
            "204-State Retirement",
            "3220",
        ],
        [
            "Health Savings Account",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        [
            "HSA  (Employee)",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        [
            "Health Savings Account Family",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        ["HSA /Family", "Health Savings Account", "207-Medical Insurance", "3401-3402"],
        [
            "HSA (family)",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        [
            "Dependent Care Flexible Spending Account",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        [
            "HSA /Individual",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        [
            "Medical Flexible Spending Account (MGIS)",
            "Health Savings Account",
            "207-Medical Insurance",
            "3401-3402",
        ],
        ["Kaiser Low", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Blue Shield HMO", "Medical", "207-Medical Insurance", "3401-3402"],
        ["UnitedHealthcare HMO", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Kaiser High", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Blue Shield PPO", "Medical", "207-Medical Insurance", "3401-3402"],
        ["UnitedHealthcare PPO", "Medical", "207-Medical Insurance", "3401-3402"],
        ["AETNA", "Medical", "207-Medical Insurance", "3401-3402"],
        ["UnitedHealthcare HDHP", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Blue Shield PRM", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Hospital Indemnity", "Medical", "207-Medical Insurance", "3401-3402"],
        ["Vision Service Plan", "Vision", "207-Medical Insurance", "3401-3402"],
        ["Delta Dental DPPO", "Dental", "208-Dental Insurance", "3401-3402"],
        ["Delta Dental DMO", "Dental", "208-Dental Insurance", "3401-3402"],
        [
            "Employer Medicare",
            "Medicare & Soc Sec",
            "212-Employer Medicare",
            "3301-3302",
        ],
        [
            "Employee Assistance Program",
            "Additional",
            "299-Other Fringe Benefits",
            "3901",
        ],
        ["Benefit Rebate", "in Lieu of Benefits", "299-Other Fringe Benefits", "3904"],
        [
            "Tuition Reimbursement",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        [
            "Taxable Tuition Reimbursement",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        ["Accident", "Other Benefits", "299-Other Fringe Benefits", "3903"],
        [
            "Employee Critical Illness",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        ["Garnishment 2", "Other Benefits", "299-Other Fringe Benefits", "3903"],
        [
            "Supplemental Term Life-Child",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        [
            "Supplemental Term Life-Employee",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        [
            "Supplemental Term Life-Spouse",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
        [
            "Voluntary Short Term Disability",
            "Other Benefits",
            "299-Other Fringe Benefits",
            "3903",
        ],
    ]

    return cat_lu


def object_lookup() -> list:

    obj_lu = [
        ["Object Code", "GL Category"],
        ["1100", "1000-2000"],
        ["1110", "1000-2000"],
        ["1115", "1000-2000"],
        ["1119", "1000-2000"],
        ["1200", "1000-2000"],
        ["1300", "1000-2000"],
        ["2100", "1000-2000"],
        ["2200", "1000-2000"],
        ["2300", "1000-2000"],
        ["2400", "1000-2000"],
        ["2900", "1000-2000"],
        ["3110", "3110"],
        ["3115", "3115"],
        ["3116", "3116"],
        ["3117", "3117"],
        ["3120", "3120"],
        ["3210", "3210"],
        ["3220", "3220"],
        ["3301", "3301-3302"],
        ["3302", "3301-3302"],
        ["3401", "3401-3402"],
        ["3402", "3401-3402"],
        ["3901", "3901"],
        ["3903", "3903"],
        ["3904", "3904"],
    ]

    return obj_lu


def school_lookup() -> list:

    sch_lu = [
        ["Site Code", "School"],
        ["601", "Hanley Middle"],
        ["602", "Hanley Elementary"],
        ["603", "Coleman"],
        ["604", "East"],
        ["521", "Memphis SpEd"],
        ["500", "Memphis Regional Office"],
    ]

    return sch_lu


def resource_lookup() -> list:

    res_lu = [
        ["Resource Code", "Resource"],
        ["0000", "Unrestricted Resources"],
        ["3010", "Title I"],
        ["3310", "SpEd - Federal IDEA"],
        ["4035", "Title II"],
        ["4611", "CSP"],
        ["6015", "LEAPS"],
        ["6040", "PSSG"],
        ["6500", "SpEd - State"],
    ]

    return res_lu
