import xlsxwriter
import tkinter as tk
from tkinter import filedialog

from lookup_tables import category_lookup, resource_lookup, object_lookup, school_lookup
from lookup_secrets import employee_lookup
from paths import place_wkbk_test

dummy_list = [["8595", "3010", "601"], ["8595", "4035", "601"]]
pr_headers = [
    "Employee Number",
    "Full Name",
    "Date",
    "Description",
    "Amount",
    "DatePosting",
    "Category",
    "TN Reporting Category",
    "GL Category",
]
gl_headers = [
    "ABRA Staff ID",
    "Full Name",
    "Full Date",
    "Site Code",
    "School",
    "Object Code",
    "Object Title",
    "Resource Code",
    "Resource Title",
    "Amount",
    "DatePosted",
    "GL Category",
]
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def get_dir(start_path):
    root = tk.Tk()
    root.withdraw()
    list_files = filedialog.askdirectory(initialdir=start_path)
    return list_files


def create_wrkst_grantee(pr_gl_wkbk, abra: str, res_code: str, site_code: str):
    # get emp_FName, epm_LName using ABRA ID
    # get Grant_Name using res_code

    calibri_15_b = pr_gl_wkbk.add_format()
    calibri_15_b.set_bold()
    calibri_15_b.set_font("Calibri")
    calibri_15_b.set_font_size(15)

    calibri_11_b = pr_gl_wkbk.add_format()
    calibri_11_b.set_bold()
    calibri_11_b.set_font("Calibri")
    calibri_11_b.set_font_size(11)

    abra_wrkst = pr_gl_wkbk.add_worksheet("-".join([res_code, site_code, abra, abra]))

    [abra_wrkst.set_column(i, i, 18) for i in range(21)]
    abra_wrkst.set_column(0, 0, 2.29)
    abra_wrkst.set_column(4, 4, 2.29)
    abra_wrkst.set_column(7, 7, 2.29)
    abra_wrkst.set_column(10, 10, 2.29)
    abra_wrkst.set_column(18, 18, 2.29)
    abra_wrkst.set_column(21, 21, 2.29)

    abra_wrkst.write(
        1,
        1,
        '=MID(CELL("filename", $A$1), FIND("]", CELL("filename", $A$1))+1, 256)',
        calibri_15_b,
    )
    abra_wrkst.write(3, 2, "GL - Data - All", calibri_11_b)
    abra_wrkst.write(3, 5, "GL Data - Resource Specific", calibri_11_b)
    abra_wrkst.write(3, 8, "Payroll Data", calibri_11_b)

    return pr_gl_wkbk


def pr_formulae(row:int)->list:
    r = str(row)
    pr_formulae_mod = [
        "=IF(DAY(C"+r+")>15,EOMONTH(C"+r+",0),EOMONTH(C"+r+",-1)+15)",
        "=VLOOKUP($D"+r+",Lookups!$B:$E,2,0)",
        "=VLOOKUP($D"+r+",Lookups!$B:$E,3,0)",
        "=VLOOKUP($D"+r+",Lookups!$B:$E,4,0)",
    ]
    return pr_formulae_mod


def create_wrkst_pr(pr_gl_wkbk, abra_list: list, month_str: str, year_str: str):
    pr_headers = [
        "Employee Number",
        "Full Name",
        "Date",
        "Description",
        "Amount",
        "DatePosting",
        "Category",
        "TN Reporting Category",
        "GL Category",
    ]
    refer_formula = pr_formulae(2)

    calibri_11_b_bord = pr_gl_wkbk.add_format()
    calibri_11_b_bord.set_font("Calibri")
    calibri_11_b_bord.set_font_size(11)
    calibri_11_b_bord.set_bold()
    calibri_11_b_bord.set_bottom(2)
    calibri_11_b_bord.set_top(2)

    calibri_11_b_bord_l = pr_gl_wkbk.add_format()
    calibri_11_b_bord_l.set_font("Calibri")
    calibri_11_b_bord_l.set_font_size(11)
    calibri_11_b_bord_l.set_bold()
    calibri_11_b_bord_l.set_bottom(2)
    calibri_11_b_bord_l.set_top(2)
    calibri_11_b_bord_l.set_left(2)

    calibri_11 = pr_gl_wkbk.add_format()
    calibri_11.set_font("Calibri")
    calibri_11.set_font_size(11)

    calibri_11_blue = pr_gl_wkbk.add_format()
    calibri_11_blue.set_font("Calibri")
    calibri_11_blue.set_font_size(11)
    calibri_11_blue.set_bg_color("#DCE6F1")

    base_col_width = 17.23

    pr_wrkst = pr_gl_wkbk.add_worksheet("Payroll Data")
    [pr_wrkst.set_column(i, i, base_col_width) for i in range(12)]
    pr_wrkst.set_column(0, 0, 2.29)
    pr_wrkst.set_column(2, 2, (base_col_width * 2))
    pr_wrkst.set_column(4, 4, (base_col_width * 2))
    pr_wrkst.set_column(8, 8, (base_col_width * 2))
    pr_wrkst.set_column(3, 3, (base_col_width * (2 / 3)))
    pr_wrkst.set_column(5, 5, (base_col_width * (2 / 3)))
    pr_wrkst.set_column(6, 6, (base_col_width * (2 / 3)))
    pr_wrkst.set_column(9, 9, (base_col_width * (2 / 3)))

    [
        pr_wrkst.write(1, (i + 1 + 5), refer_formula[i], calibri_11_blue)
        for i in range(len(refer_formula))
    ]
    [pr_wrkst.write(3, i + 1, "", calibri_11_b_bord) for i in range(len(pr_headers))]
    pr_wrkst.write(3, 1, "Payroll Data", calibri_11_b_bord)
    pr_wrkst.write(3, 6, "", calibri_11_b_bord_l)
    [
        pr_wrkst.write(5, i + 1, pr_headers[i], calibri_11)
        for i in range(len(pr_headers))
    ]

    return pr_gl_wkbk


def write_array(wkst, x: int, y: int, data_tab: list, frmt):
    for i in range(0, len(data_tab[0])):
        for j in range(0, len(data_tab)):
            wkst.write((y+j),(x+i),data_tab[j][i],frmt)
            print(str(i)+", "+ str(j))


def create_wrkst_lu(pr_gl_wkbk, abra_list: list, month_str: str, year_str: str):

    calibri_11_b_bord = pr_gl_wkbk.add_format()
    calibri_11_b_bord.set_font("Calibri")
    calibri_11_b_bord.set_font_size(11)
    calibri_11_b_bord.set_bold()
    calibri_11_b_bord.set_bottom(2)
    calibri_11_b_bord.set_top(2)

    calibri_11_b_bord_l = pr_gl_wkbk.add_format()
    calibri_11_b_bord_l.set_font("Calibri")
    calibri_11_b_bord_l.set_font_size(11)
    calibri_11_b_bord_l.set_bold()
    calibri_11_b_bord_l.set_bottom(2)
    calibri_11_b_bord_l.set_top(2)
    calibri_11_b_bord_l.set_left(2)

    calibri_11_b_bord_r = pr_gl_wkbk.add_format()
    calibri_11_b_bord_r.set_font("Calibri")
    calibri_11_b_bord_r.set_font_size(11)
    calibri_11_b_bord_r.set_bold()
    calibri_11_b_bord_r.set_bottom(2)
    calibri_11_b_bord_r.set_top(2)
    calibri_11_b_bord_r.set_right(2)

    calibri_11 = pr_gl_wkbk.add_format()
    calibri_11.set_font("Calibri")
    calibri_11.set_font_size(11)

    calibri_11_blue = pr_gl_wkbk.add_format()
    calibri_11_blue.set_font("Calibri")
    calibri_11_blue.set_font_size(11)
    calibri_11_blue.set_bg_color("#DCE6F1")

    lu_wrkst = pr_gl_wkbk.add_worksheet("Lookups")
    res_lu = resource_lookup()
    obj_lu = object_lookup()
    sch_lu = school_lookup()
    cat_lu = category_lookup()
    emp_lu = employee_lookup()

    write_array(lu_wrkst, 1, 5, cat_lu, calibri_11)
    write_array(lu_wrkst, 6, 5, obj_lu, calibri_11)
    write_array(lu_wrkst, 9, 5, emp_lu, calibri_11)
    write_array(lu_wrkst, 12, 5, sch_lu, calibri_11)
    write_array(lu_wrkst, 15, 5, res_lu, calibri_11)

    # [lu_wrkst.write( for j in i for i in cat_lu[i])]


def create_wkbk(abra_res_sit: list, path: str):
    month = "07"
    year = "2018"
    monthly_reim = xlsxwriter.Workbook(
        path + r"\Documentation - Payroll & GL Data - " + year + "." + month + ".xlsx"
    )

    # res_lu = resource_lookup()

    [create_wrkst_grantee(monthly_reim, i[0], i[1], i[2]) for i in abra_res_sit]
    create_wrkst_pr(monthly_reim, abra_res_sit, month, year)
    create_wrkst_lu(monthly_reim, abra_res_sit, month, year)
    # create_wrkst_check(monthly_reim, abra_res_sit, month, year)
    # create_wrkst_gl(monthly_reim, abra_res_sit, month, year)
    # create_wrkst_template(monthly_reim, abra_res_sit, month, year)

    monthly_reim.close()


def main():
    directory = get_dir(
        place_wkbk_test
    )
    create_wkbk(dummy_list, directory)


if __name__ == "__main__":
    main()
