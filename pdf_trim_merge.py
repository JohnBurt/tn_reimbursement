import random
import string
import os
import tkinter as tk
from tkinter import filedialog

from PyPDF2 import PdfFileMerger, PdfFileWriter, PdfFileReader

from pr_search import grantees_list
from paths import place_wkbk

school = 602
grant = 3010
year = "2018"
month = "08"

# PATHtoHERE : C:\Users\jburt\AppData\Local\Programs\Python\Python36\Scripts\PDF_TrimMerge.py
# python.exe PATH : C:\Users\jburt\AppData\Local\Programs\Python\Python36\python.exe

PATH = place_wkbk


def pdf_trim(pdf_file: str, keep_list: list):
    pdf_fileobj = open(pdf_file, "rb")
    trim_pdf = PdfFileWriter()
    pdf_reader = PdfFileReader(pdf_file)
    for page_num in range(1, pdf_reader.getNumPages()):
        page_obj = pdf_reader.getPage(page_num)
        page_text = page_obj.extractText()
        keep = 0
        for word_ in keep_list:
            if word_ in page_text:
                keep = 1
        if keep == 1:
            trim_pdf.addPage(page_obj)
    pdf_fileobj.close()
    return trim_pdf


def get_files(starter_path: str) -> tuple:
    root = tk.Tk()
    root.withdraw()
    list_Files = filedialog.askopenfilenames(
        initialdir=starter_path,
        title="Select file",
        filetypes=(("PDF files", "*.pdf"), ("all files", "*.*")),
    )
    return list_Files


def put_files(end_path: str) -> str:
    root = tk.Tk()
    root.withdraw()
    save_File = filedialog.asksaveasfilename(
        initialdir=end_path,
        title="Save file",
        filetypes=(("PDF files", "*.pdf"), ("all files", "*.*")),
    )
    return save_File


def elapsed(some_function):
    import time

    """ This decorator function can be used to log the execution time
    of a function. To call it, simple place @elapsed above your
    function declaration. It will print out the run time in seconds.
    """

    def wrapper(*args, **kwargs):
        start = time.time()
        some_function(*args, **kwargs)
        end = time.time()
        elapsed = round(end - start, 3)
        print(f"{some_function.__name__} completed in {elapsed} seconds")

    return wrapper


def rand_str_gen(enn: int) -> int:
    return "".join(
        random.choice(string.ascii_uppercase + string.digits) for _ in range(enn)
    )


def single_grant(files_path: str, sit_code: int, res_code: int):
    PATH = files_path[0][: (files_path[0].rfind(r"/"))]
    pdfList = list()
    staff_id_list = grantees_list(sit_code, res_code)
    donePDF = (
        files_path[0][: (files_path[0].rfind(r"/"))]
        + "\\"
        + str(sit_code)
        + " - "
        + str(res_code)
        + " - Payroll - "
        + year
        + "."
        + month
    )
    # -------------------------------------------------------------------------#
    for pdf in files_path:
        writerTrimmed = pdf_trim(pdf, staff_id_list)
        randName = PATH + "\\" + "0" + rand_str_gen(15) + ".pdf"
        pdfList.append(randName)
        with open(randName, "wb") as pdfOut:
            writerTrimmed.write(pdfOut)
    # -------------------------------------------------------------------------#
    merger = PdfFileMerger()
    for pdf in pdfList:
        merger.append(pdf)
    print(donePDF)
    with open(donePDF + ".pdf", "wb") as fout:
        merger.write(fout)
    merger.close()
    for file in pdfList:
        os.remove(file)


@elapsed
def main():
    get_pdfs = get_files(PATH)
    single_grant(get_pdfs, school, grant)


if __name__ == "__main__":
    main()
