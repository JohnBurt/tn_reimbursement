import pyodbc as dBase
from datetime import date
from dateutil.relativedelta import relativedelta
import tkinter as tk
from tkinter import filedialog
import time


def elapsed(some_function):

    """ 
    This decorator function can be used to log the execution time
    of a function. To call it, simple place @elapsed above your
    function declaration. It will print out the run time in seconds.
    """

    def wrapper(*args, **kwargs):
        start = time.time()
        some_function(*args, **kwargs)
        end = time.time()
        elapsed = round(end - start, 2)
        print(f"{some_function.__name__} completed in {elapsed} seconds")

    return wrapper


def getFiles(starterPath: str) -> str:
    root = tk.Tk()
    root.withdraw()
    list_Files = filedialog.askopenfilenames(
        initialdir=starterPath,
        title="Select file",
        filetypes=(("PDF files", "*.pdf"), ("all files", "*.*")),
    )
    return list_Files


def get_query_gl(list_grantees):
    qry_gl = f"""SELECT AspireDW.dbo.dimStaff.ABRAStaffID AS 'ABRA Staff ID',
                        CONCAT(AspireDW.dbo.dimStaff.FirstName,' ',AspireDW.dbo.dimStaff.LastName) AS 'Full Name',
                        AspireDW.dbo.dimDate.FullDate AS 'Full Date',
                        AspireDW.dbo.dimAccountObject.ObjectCode AS 'Object Code',
                        AspireDW.dbo.dimAccountObject.ObjectTitle AS 'Object Title',
                        AspireDW.dbo.dimAccountResource.ResourceCode AS 'Resource Code',
                        AspireDW.dbo.dimAccountResource.ResourceTitle AS 'Resource Title',
                        _Trans.Amount AS 'Amount'

                FROM AspireDW.dbo.factAccountingTrans AS _Trans
                        LEFT JOIN AspireDW.dbo.dimAccountingTransType ON dimAccountingTransType.AccountingTransTypeKEY = _Trans.AccountingTransTypeKEY
                        LEFT JOIN AspireDW.dbo.dimAccountResource ON dimAccountResource.AccountResourceKEY = _Trans.AccountResourceKEY
                        LEFT JOIN AspireDW.dbo.dimAccountObject ON dimAccountObject.AccountObjectKEY = _Trans.AccountObjectKEY
                        LEFT JOIN AspireDW.dbo.dimAccountSite ON dimAccountSite.AccountSiteKEY = _Trans.AccountSiteKEY
                        LEFT JOIN AspireDW.dbo.dimDate ON dimDate.DateKEY = _Trans.DateKEY
                        LEFT JOIN AspireDW.dbo.dimStaff ON dimStaff.StaffKEY = _Trans.StaffKEY
                WHERE 1=1
                        AND AspireDW.dbo.dimAccountingTransType.RowIsCurrent = 'Y'
                        AND AspireDW.dbo.dimAccountingTransType.TransactionType = 'General Ledger'
                        AND AspireDW.dbo.dimAccountingTransType.TransactionSubType = '-----'
                        AND AspireDW.dbo.dimAccountResource.RowIsCurrent = 'Y'
                        AND AspireDW.dbo.dimAccountObject.RowIsCurrent = 'Y'
                        AND AspireDW.dbo.dimAccountSite.RowIsCurrent = 'Y'

                        AND AspireDW.dbo.dimStaff.ABRAStaffID IN (
                            {list_grantees}
                        )
                        AND AspireDW.dbo.dimDate.FiscalYearShort = '18-19'
                        AND MONTH(AspireDW.dbo.dimDate.FullDate) = MONTH(DATEADD(month, -1, GETDATE()))
                        AND AspireDW.dbo.dimAccountObject.ProfitAndLoss_Code IN (
                                '1000', '2000', '3000'
                        )
                ORDER BY AspireDW.dbo.dimStaff.ABRAStaffID, AspireDW.dbo.dimDate.FullDate, AspireDW.dbo.dimAccountObject.ObjectCode"""
    return qry_gl


def get_query_pr(list_grantees, first_of_month, last_of_month):
    qry_pr = f"""SELECT USEarn.EmployeeNumber AS 'Employee Number',
                        CONCAT(USEarn.NameFirst, ' ', USEarn.NameLast) AS 'Full Name',
                        USEarn.PayDate AS 'Date',
                        USEarn.Earnings AS 'Description',
                        USEarn.PeriodAmount AS 'Amount'
                FROM UltiProStaged.dbo.vw_PayrollHistory_Earnings AS USEarn
                WHERE 1=1
                        AND USEarn.EmployeeNumber IN (
                                {list_grantees}
                        )
                        AND USEarn.PayDate > '{first_of_month} 00:00:00.000'
                        AND USEarn.PayDate < '{last_of_month} 00:00:00.000'
                        AND	USEarn.PeriodAmount != '0'
                UNION
                SELECT USTax.EmployeeNumber,
                        CONCAT(USTax.NameFirst, ' ', USTax.NameLast) AS FullName,
                        USTax.PayDate,
                        USTax.TaxDesc,
                        USTax.CurrentTaxAmount
                FROM UltiProStaged.dbo.vw_PayrollHistory_Taxes AS USTax
                WHERE 1=1
                        AND USTax.EmployeeNumber IN (
                                {list_grantees}
                        )
                        AND USTax.PayDate > '{first_of_month} 00:00:00.000'
                        AND USTax.PayDate < '{last_of_month} 00:00:00.000'
                        AND USTax.TaxDesc NOT IN (
                        'Social Security Employee Tax',
                        'Employee Medicare',
                        'Federal Income Tax'
                        )
                        AND	USTax.CurrentTaxAmount != '0'
                UNION
                SELECT USDed.EmployeeNumber,
                        CONCAT(USDed.NameFirst, ' ', USDed.NameLast) AS FullName,
                        USDed.DatePay,
                        USDed.DeductionDesc,
                        USDed.EmployerAmount
                FROM UltiProStaged.dbo.vw_PayrollHistory_Deductions AS USDed
                WHERE 1=1
                        AND USDed.EmployeeNumber IN (
                                {list_grantees}
                        )
                        AND USDed.DatePay > '{first_of_month} 00:00:00.000'
                        AND USDed.DatePay < '{last_of_month} 00:00:00.000'
                        AND USDed.DeductionDesc NOT IN (
                        'HSA  (Employee)'
                        )
                        AND USDed.EmployerAmount != '0';"""
    return qry_pr


def query_gl(abras: list) -> list:

    dw_conn = dBase.connect(
        r"DRIVER={SQL Server};"
        r"SERVER=dw;"
        r"DATABASE=master;"
        r"Trusted_Connection=yes;"
    )
    cur = dw_conn.cursor()

    gl_tab = []
    cur.execute(get_query_gl(abras))
    for row in cur.fetchall():
        gl_tab.append([i for i in row])

    return gl_tab


def query_pr(abras: list) -> list:
    today = date.today()
    d = today - relativedelta(months=1)
    beg_month = date(d.year, d.month, 1)
    end_month = date(today.year, today.month, 1) - relativedelta(days=1)

    dw_conn = dBase.connect(
        r"DRIVER={SQL Server};"
        r"SERVER=dw;"
        r"DATABASE=master;"
        r"Trusted_Connection=yes;"
    )
    cur = dw_conn.cursor()

    pr_tab = []
    cur.execute(get_query_pr(abras, beg_month, end_month))
    for row in cur.fetchall():
        pr_tab.append([i for i in row])

    return pr_tab


@elapsed
def main():
    today = date.today()
    d = today - relativedelta(months=1)
    beg_month = date(d.year, d.month, 1)
    end_month = date(today.year, today.month, 1) - relativedelta(days=1)

    list_grantees = [
        "5641",
        "6812",
        "7898",
        "09570",
        "8223",
        "8703",
        "8603",
        "6178",
        "8597",
        "7363",
        "7825",
        "7964",
        "7790",
        "7822",
        "8214",
        "8703",
        "7040",
        "5563",
        "5562",
        "5539",
        "09361",
        "8971",
        "09258",
        "09712",
        "5566",
        "6270",
        "7796",
        "6263",
        "6485",
        "2145",
        "2905",
        "6271",
        "6258",
    ]
    list_grantees_str = ", ".join(["'" + i + "'" for i in list_grantees])
    # list_grantees_int = [int(i) for i in list_grantees]

    dw_conn = dBase.connect(
        r"DRIVER={SQL Server};"
        r"SERVER=dw;"
        r"DATABASE=master;"
        r"Trusted_Connection=yes;"
    )
    cur = dw_conn.cursor()

    gl_tab = []
    cur.execute(get_query_gl(list_grantees_str))
    for row in cur.fetchall():
        gl_tab.append([i for i in row])
    pr_tab = []
    cur.execute(get_query_pr(list_grantees_str, beg_month, end_month))
    for row in cur.fetchall():
        pr_tab.append([i for i in row])

    cur.close()
    dw_conn.close


if __name__ == "__main__":
    main()
