import tkinter as tk
from tkinter import filedialog

import pdf_trim_merge
from pdf_trim_merge import single_grant
from paths import place_pdfs

PATH = place_pdfs

def get_pr_files(starter_path: str) -> tuple:
    root = tk.Tk()
    root.withdraw()
    list_Files = filedialog.askopenfilenames(
        initialdir=starter_path,
        title="Select file",
        filetypes=(("PDF files", "*.pdf"), ("all files", "*.*")),
    )
    return list_Files


school_list = [601, 602, 603]
grant_list = [3010, 3310, 4035, 5000, 6015, 6040, 6500]
# combined = [[x, y] for x in school_list for y in grant_list]
combined = [
    [601, 3010],
    [601, 3310],
    [601, 4035],
    [601, 6040],
    [602, 3010],
    [602, 3310],
    [602, 4035],
    [602, 6015],
    [602, 6040],
    [603, 3010],
    [603, 3310],
    [603, 4035],
    [603, 6040],
]

pr_files_ = get_pr_files(PATH)

[single_grant(pr_files_, x[0], x[1]) for x in combined]
